const express = require('express');
const path = require('path');

const app = express();

// Serve static files from the 'public' directory
app.use(express.static(path.join(__dirname, 'public')));

// Serve script.js from the root directory
app.get('/script.js', (req, res) => {
    res.sendFile(path.join(__dirname, 'script.js'));
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'index.html'));
});
app.post('/save-annotations', (req, res) => {
    const { person, fileName, data } = req.body;
    const filePath = path.join(__dirname, 'data', `${fileName}.json`);
    
    fs.writeFile(filePath, JSON.stringify(data, null, 2), (err) => {
        if (err) {
            return res.status(500).send('Error saving file');
        }
        res.status(200).send('File saved successfully');
    });
});
app.listen(3000, () => {
    console.log('Server is running on port 3000');
});
