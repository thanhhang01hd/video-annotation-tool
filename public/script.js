let video = document.getElementById('video');
let videoSource = document.getElementById('videoSource');
let customProgress = document.getElementById('customProgress');
let progressBar = document.getElementById('progressBar');
let marker = document.createElement('div');
marker.className = 'marker';
customProgress.appendChild(marker);
let currentActions = [];
let interval;
let annotations = {
    person1: [],
    person2: []
};

const actions = [
    "Looking at interlocutor", 
    "Head Nods", 
    "Head Shake", 
    "Frowning", 
    "Eyebrow-raising", 
    "Smile", 
    "Laughter", 
    "Leaning forward", 
    "Leaning backward"
];

function loadVideo(event) {
    let file = event.target.files[0];
    if (file) {
        let url = URL.createObjectURL(file);
        videoSource.src = url;
        video.load();
    }
}

video.addEventListener('timeupdate', () => {
    let percent = (video.currentTime / video.duration) * 100;
    progressBar.style.width = percent + '%';
    marker.style.left = percent + '%';
});

customProgress.addEventListener('click', seekVideo);

function seekVideo(event) {
    const rect = customProgress.getBoundingClientRect();
    const posX = event.clientX - rect.left;
    const percent = posX / rect.width;
    video.currentTime = percent * video.duration;
}

video.addEventListener('play', () => {
    video.addEventListener('timeupdate', updateTable);
});

function startAction(button, person, actionType) {
    let currentAction = currentActions.find(action => action.person === person && action.actionType === actionType);
    if (currentAction) {
        currentAction.endTime = getCurrentTimeInCentiseconds(video.currentTime);
        addAnnotation(currentAction.person, currentAction.actionType, currentAction.startTime, currentAction.endTime);
        currentActions = currentActions.filter(action => !(action.person === person && action.actionType === actionType));
        button.classList.remove('active'); // Reset button color
    } else {
        currentActions.push({
            person: person,
            actionType: actionType,
            startTime: getCurrentTimeInCentiseconds(video.currentTime),
            endTime: null
        });
        button.classList.add('active'); // Change button color to active
    }
}

function initializeActionButtons(person) {
    let buttonGroup = document.querySelector(`#buttons-${person.toLowerCase().replace(' ', '-')} .action-buttons`);
    actions.forEach(actionType => {
        let newButton = document.createElement('button');
        newButton.textContent = actionType;
        newButton.onclick = function() { startAction(this, person, actionType); };
        buttonGroup.appendChild(newButton);
    });
}

function addNewActionButton(person) {
    let actionType = prompt(`Enter the new action type for ${person}:`);
    if (actionType) {
        let buttonGroup = document.querySelector(`#buttons-${person.toLowerCase().replace(' ', '-')} .action-buttons`);
        let newButton = document.createElement('button');
        newButton.textContent = `${actionType}`;
        newButton.onclick = function() { startAction(this, person, actionType); };
        buttonGroup.appendChild(newButton);
    }
}

function addAnnotation(person, actionType, start, end) {
    let tableId = `annotations-${person.toLowerCase().replace(' ', '-')}`;
    let annotationsTable = document.getElementById(tableId);
    let row = document.createElement('tr');
    row.dataset.start = start;
    row.dataset.end = end;
    row.dataset.person = person;
    let actionCell = document.createElement('td');
    actionCell.textContent = actionType;
    let startCell = document.createElement('td');
    startCell.textContent = formatCentiseconds(start);
    let endCell = document.createElement('td');
    endCell.textContent = formatCentiseconds(end);
    let remarksCell = document.createElement('td');
    let remarksInput = document.createElement('textarea');
    remarksInput.className = 'remarks';
    remarksCell.appendChild(remarksInput);
    let playCell = document.createElement('td');
    let playButton = document.createElement('button');
    playButton.textContent = 'Play';
    playButton.onclick = function() {
        playSegment(start, end);
    };
    playCell.appendChild(playButton);
    let deleteCell = document.createElement('td');
    let deleteButton = document.createElement('button');
    deleteButton.textContent = 'Delete';
    deleteButton.onclick = function() { 
        annotationsTable.removeChild(row);
        removeAnnotation(person, start, end);
    };
    deleteCell.appendChild(deleteButton);
    row.appendChild(actionCell);
    row.appendChild(startCell);
    row.appendChild(endCell);
    row.appendChild(remarksCell);
    row.appendChild(playCell);
    row.appendChild(deleteCell);
    annotationsTable.prepend(row); // Add new row to the top

    // Update annotations array
    annotations[person.toLowerCase().replace(' ', '')].push({ person, actionType, start, end });

    // Render the progress bar immediately
    renderCustomProgress();
}

function playSegment(start, end) {
    clearInterval(interval);
    let startTime = start / 100;
    let endTime = end / 100;
    video.currentTime = startTime;
    video.play();
    interval = setInterval(() => {
        if (video.currentTime >= endTime) {
            video.pause();
            clearInterval(interval);
        }
    }, 10); // Check every 10 milliseconds
}

function saveAllAnnotations() {
    saveAnnotations('Person 1');
    saveAnnotations('Person 2');
    renderCustomProgress();
}

function saveAnnotations(person) {
    let tableId = `annotations-${person.toLowerCase().replace(' ', '-')}`;
    let annotationsTable = document.getElementById(tableId);
    let data = [];
    for (let row of annotationsTable.rows) {
        let cells = row.cells;
        if (cells.length === 6) {
            data.push({
                action: cells[0].textContent,
                startTime: cells[1].textContent,
                endTime: cells[2].textContent,
                remarks: cells[3].firstChild.value
            });
        }
    }
    let json = JSON.stringify(data);
    let blob = new Blob([json], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let a = document.createElement('a');
    a.href = url;
    a.download = `${person.toLowerCase().replace(' ', '_')}_annotations.json`;
    a.click();
    URL.revokeObjectURL(url);
}

function getCurrentTimeInCentiseconds(timeInSeconds) {
    return Math.floor(timeInSeconds * 100);
}

function formatCentiseconds(centiseconds) {
    let totalSeconds = Math.floor(centiseconds / 100);
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;
    let cs = centiseconds % 100;
    return `${minutes < 10 ? '0' : ''}${minutes}:${seconds < 10 ? '0' : ''}${seconds}:${cs < 10 ? '0' : ''}${cs}`;
}

function renderCustomProgress() {
    customProgress.innerHTML = '';
    customProgress.appendChild(progressBar);
    customProgress.appendChild(marker);
    let totalDuration = video.duration * 100; // Convert to centiseconds
    let combinedAnnotations = [...annotations.person1, ...annotations.person2];

    combinedAnnotations.forEach(annotation => {
        let segment = document.createElement('div');
        segment.classList.add('timeline-segment');
        segment.style.left = `${(annotation.start / totalDuration) * 100}%`;
        segment.style.width = `${((annotation.end - annotation.start) / totalDuration) * 100}%`;

        let isPerson1 = annotations.person1.some(a => a.start === annotation.start && a.end === annotation.end);
        let isPerson2 = annotations.person2.some(a => a.start === annotation.start && a.end === annotation.end);

        if (isPerson1 && isPerson2) {
            segment.style.backgroundColor = 'red';
        } else if (isPerson1) {
            segment.style.backgroundColor = 'violet';
        } else if (isPerson2) {
            segment.style.backgroundColor = 'blue';
        }

        segment.addEventListener('click', () => highlightTableRows(annotation.start, annotation.end));

        let tooltip = document.createElement('div');
        tooltip.classList.add('tooltip');
        tooltip.textContent = `${annotation.actionType} - ${annotation.person}`;
        segment.appendChild(tooltip);

        customProgress.appendChild(segment);
    });

    document.addEventListener('click', resetHighlights);
}

function highlightTableRows(start, end) {
    resetHighlights();
    const rows = document.querySelectorAll(`tr[data-start='${start}'][data-end='${end}']`);
    rows.forEach(row => row.classList.add('highlight'));
}

function resetHighlights(event) {
    if (!event || !event.target.closest('.timeline-segment')) {
        const highlightedRows = document.querySelectorAll('tr.highlight');
        highlightedRows.forEach(row => row.classList.remove('highlight'));
    }
}

function removeAnnotation(person, start, end) {
    annotations[person.toLowerCase().replace(' ', '')] = annotations[person.toLowerCase().replace(' ', '')].filter(annotation => !(annotation.start === start && annotation.end === end));
    renderCustomProgress();
}

function changePlaybackRate() {
    let playbackRate = document.getElementById('playbackRate').value;
    video.playbackRate = playbackRate;
}

// Initialize buttons for Person 1 and Person 2
initializeActionButtons('Person 1');
initializeActionButtons('Person 2');
